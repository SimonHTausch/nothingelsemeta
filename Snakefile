# Rules ----------------------------------------------


rule all:
  input:
    expand	#todo: define list of samples


rule download_sra:
  input: 
    #todo: define input
  output:
    r1 = "download_temp/{sample}_R1.fastq.gz",
    r2 = "download_temp/{sample}_R2.fastq.gz"
  message:
    "Downloading {wildcards.sample}."
  log:
    "logs/download_{sample}.log"
  shell:
    "wget " #todo: define download line"


rule run_fastp:
  input:
    r1 = "download_temp/{sample}_R1.fastq.gz",
    r2 = "download_temp/{sample}_R2.fastq.gz"
  output:
    r1 = "trimmed/{sample}_R1.fastq.gz",
    r2 = "trimmed/{sample}_R2.fastq.gz",
    json = "trimmed/reports/{sample}.json",
    html = "trimmed/reports/{sample}.html"
  message: "Running fastp on {wildcards.sample}"
  conda:
   "envs/fastp.yaml"
  log:
   "logs/fastp_{sample}.log"
  threads:
    config["params"]["threads"]
    params:
        length_required = config["params"]["fastp"]["length_required"]
    shell:
        "fastp --in1 {input.r1} --out1 {output.r1} --in2 {input.r2} --out2 {output.r2} --detect_adapter_for_pe --json {output.json} --html {output.html} --thread {threads} --report_title 'Sample {wildcards.sample}' --verbose --length_required {params.length_required} 2>&1 | tee {log}"


rule run_mash:
  input: 
    r1 = "trimmed/{sample}_R1.fastq.gz",
    r2 = "trimmed/{sample}_R2.fastq.gz"   
  output: 
    distance = "trimmed/{sample}_mash.dist"
  params: 
    sketch = "Assembly/{sample}/mash", # only needed if two commands
    mashDB=config["params"]["mash_refdb"],
    kmersize=config["params"]["mash_kmersize"], #21
    sketchsize=config["params"]["mash_sketchsize"] #1000
  conda: 
    "envs/mash.yaml"
  threads:
    config["params"]["threads"]
  message:
    "Running rule run_mash on {wildcards.sample}"
  log:
    "logs/{sample}_mash.log"
  shell: 
    """
    mash sketch -p {threads} -k {params.kmersize} -s {params.sketchsize} -o {params.sketch} {input} 2>&1 | tee {log}
    mash dist {params.mashDB} {params.sketch}.msh > {output.distance}  2>&1 | tee -a {log}
    """

